<?
namespace Rezolve\Calculator\Test\Api;

use Rezolve\Calculator\Model\Calculator;
use Magento\Framework\Webapi\Rest\Request;
use Magento\TestFramework\TestCase\WebapiAbstract;

class CalculatorTest extends WebapiAbstract
{
    /**
     * Service constants
     */
    const RESOURCE_PATH = '/V1/calculator/result';
    const SERVICE_NAME = 'rezolveCalculatorV1';

    public function testCalculator()
    {
        $left = 1;
        $right = 2;
        $operator = 'add';
        $precision = 3;
        $serviceInfo = [
            'rest' => [
                'resourcePath' => self::RESOURCE_PATH,
                'httpMethod' => Request::HTTP_METHOD_POST,
            ],
        ];
        $requestData = [
            'left' => $left,
            'right' => $right,
            'operator' => $operator,
            'precision' => $precision
        ];
        $jsonData = $this->_webApiCall($serviceInfo, $requestData);
        $jsonData = \Zend_Json::decode($jsonData);
        $result = Calculator::RESPONSE_LABEL_RESULT;
        $this->assertEquals(3, $jsonData->$result);
    }
}
