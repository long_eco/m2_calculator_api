<?php
namespace Rezolve\Calculator\Plugin\Magento\Framework\Webapi\Rest;

use Magento\Framework\Oauth\Helper\Request;
use Magento\Framework\Webapi\Rest\Response as mResponse;
use Magento\Webapi\Controller\Rest\InputParamsResolver;

class Response
{
    /**
     * @var InputParamsResolver
     */
    private $inputParamsResolver;

    /**
     * Response constructor.
     * @param InputParamsResolver $inputParamsResolver
     */
    public function __construct(InputParamsResolver $inputParamsResolver)
    {
        $this->inputParamsResolver = $inputParamsResolver;
    }

    /**
     * @param \Magento\Framework\Webapi\Rest\Response $subject
     * @param \Closure $proceed
     * @param null $outputData
     * @return mixed
     */
    public function aroundPrepareResponse(mResponse $subject, \Closure $proceed, $outputData = null)
    {
        $obj = $proceed($outputData);
        $route = $this->inputParamsResolver->getRoute();
        $serviceClassName = $route->getServiceClass();
        if ($serviceClassName == 'Rezolve\Calculator\Api\CalculatorInterface') :
            $obj->setBody($outputData);
            $subject->setHttpResponseCode(Request::HTTP_OK);
            $obj->setHeader('Content-type', 'application/json', true);
        endif;

        return $obj;
    }
}
